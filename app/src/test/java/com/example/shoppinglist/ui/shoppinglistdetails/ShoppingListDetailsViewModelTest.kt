package com.example.shoppinglist.ui.shoppinglistdetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.shoppinglist.data.model.ShoppingList
import com.example.shoppinglist.data.model.ShoppingListDetails
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class ShoppingListDetailsViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ShoppingListDetailsViewModel

    @Before
    fun setup() {
        viewModel = ShoppingListDetailsViewModel()
    }

    @Test
    fun insertDataToDatabaseTest() {
        //when
        viewModel.insertDataToDatabase()
        //then
        Assert.assertTrue(viewModel.callShoppingDetailsAddDialog.value == true)
    }

    @Test
    fun shoppingListAddCountAllTest() {
        //given
        val item = ShoppingList(0, "name", true, "2021-05-09", 0, 0)
        //when
        viewModel.shoppingListAddCountAll(item)
        //then
        Assert.assertEquals(item.countAll, 1)
    }

    @Test
    fun updateShoppingListDetailsByDoneTest() {
        //given
        val item = ShoppingListDetails(0, 1, "name", 3, true)
        //when
        viewModel.updateShoppingListDetailsByDone(item)
        //then
        Assert.assertTrue(viewModel.callShoppingListDoneDetails.value == true)
    }

    @Test
    fun shoppingListDoneCountAllFlagTrueTest() {
        //given
        val item = ShoppingList(0, "name", true, "2021-05-09", 0, 0)
        val flag = true
        //when
        viewModel.shoppingListDoneCountAll(item, flag)
        //then
        Assert.assertEquals(item.countDone, 1)
    }

    @Test
    fun shoppingListDoneCountAllFlagFalseTest() {
        //given
        val item = ShoppingList(0, "name", true, "2021-05-09", 1, 0)
        val flag = false
        //when
        viewModel.shoppingListDoneCountAll(item, flag)
        //then
        Assert.assertEquals(item.countDone, 0)
    }

    @Test
    fun swipeShoppingDetailsDialogTest() {
        //given
        val item = ShoppingListDetails(0, 1, "name", 3, true)
        //when
        viewModel.swipeShoppingDetailsDialog(item)
        //then
        Assert.assertEquals(viewModel.callShoppingDetailsDeleteDialog.value, item)
    }

    @Test
    fun shoppingListDetailsDeleteTest() {
        //given
        val item = ShoppingListDetails(0, 1, "name", 3, true)
        //when
        viewModel.shoppingListDetailsDelete(item)
        //then
        Assert.assertEquals(viewModel.callShoppingListDeleteDetails.value, item)
    }

    @Test
    fun shoppingListDetailsDeleteFromShoppingListDoneTrueTest() {
        //given
        val item = ShoppingList(0, "name", true, "2021-05-09", 1, 1)
        val item1 = ShoppingListDetails(0, 1, "name", 3, true)
        //when
        viewModel.shoppingListDetailsDeleteFromShoppingList(item, item1)
        //then
        Assert.assertEquals(item.countAll, 0)
        Assert.assertEquals(item.countDone, 0)
    }

    @Test
    fun shoppingListDetailsDeleteFromShoppingListDoneFalseTest() {
        //given
        val item = ShoppingList(0, "name", true, "2021-05-09", 1, 1)
        val item1 = ShoppingListDetails(0, 1, "name", 3, false)
        //when
        viewModel.shoppingListDetailsDeleteFromShoppingList(item, item1)
        //then
        Assert.assertEquals(item.countAll, 0)
        Assert.assertEquals(item.countDone, 1)
    }

    @Test
    fun addListDetailsTest() {
        //given
        viewModel.ID = 1
        viewModel.ShoppingListDetailsName = "name"
        viewModel.ShoppingListDetailsCount = 1
        //when
        viewModel.addListDetails()
        //then
        Assert.assertTrue(viewModel.callShoppingListAddDetails.value == true)
    }
}