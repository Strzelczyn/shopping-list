package com.example.shoppinglist.ui.shoppinglistdetails

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.R
import com.example.shoppinglist.data.model.ShoppingListDetails
import kotlinx.android.synthetic.main.layout_shopping_list_details.view.*


class ShoppingListDetailsAdapter(private val onClik: (ShoppingListDetails) -> Unit) :
    RecyclerView.Adapter<ShoppingListDetailsAdapter.ShoppingListDetailsHolder>() {

    var archived: Boolean = false

    private var shoppingListDetails = emptyList<ShoppingListDetails>()

    class ShoppingListDetailsHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListDetailsHolder {
        return ShoppingListDetailsHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_shopping_list_details, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return shoppingListDetails.size
    }

    override fun onBindViewHolder(holder: ShoppingListDetailsHolder, position: Int) {
        val currentItem = shoppingListDetails[position]
        holder.itemView.checkBox.isChecked = currentItem.done
        if (holder.itemView.checkBox.isChecked) {
            holder.itemView.shoppingListDetailsTextView.paintFlags =
                holder.itemView.shoppingListDetailsTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            holder.itemView.shoppingListDetailsCountTextView.paintFlags =
                holder.itemView.shoppingListDetailsCountTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            holder.itemView.shoppingListDetailsTextView.paintFlags =
                holder.itemView.shoppingListDetailsTextView.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            holder.itemView.shoppingListDetailsCountTextView.paintFlags =
                holder.itemView.shoppingListDetailsCountTextView.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }

        holder.itemView.shoppingListDetailsTextView.text = currentItem.name
        holder.itemView.shoppingListDetailsCountTextView.text = "x${currentItem.count}"

        if (!archived) {
            holder.itemView.checkBox.setOnClickListener {
                currentItem.done = !currentItem.done
                onClik.invoke(currentItem)
            }
        } else {
            holder.itemView.checkBox.isEnabled = false
        }
    }

    fun setData(details: List<ShoppingListDetails>) {
        this.shoppingListDetails = details
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ShoppingListDetails {
        return shoppingListDetails[position]
    }
}
