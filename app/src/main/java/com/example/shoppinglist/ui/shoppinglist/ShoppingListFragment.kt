package com.example.shoppinglist.ui.shoppinglist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.shoppinglist.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ShoppingListFragment : Fragment() {

    companion object {
        const val PLACE_HOLDER_SECTION_1 = 1
        const val PLACE_HOLDER_SECTION_2 = 2
    }

    private val listFragment = listOf(
        FragmentItem(
            R.drawable.ic_baseline_format_list_bulleted_24,
            R.string.shopping_lists
        ) { PlaceholderFragment.newInstance(PLACE_HOLDER_SECTION_1) },
        FragmentItem(
            R.drawable.ic_baseline_archive_24,
            R.string.archived_shopping_lists
        ) { PlaceholderFragment.newInstance(PLACE_HOLDER_SECTION_2) }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shopping_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, listFragment)
        val viewPager: ViewPager2 = view.findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = view.findViewById(R.id.tabs)

        TabLayoutMediator(tabs, viewPager) { tab, position ->
            val itemFragment = listFragment[position]
            tab.setText(itemFragment.titleStringRes)
            tab.setIcon(itemFragment.iconDrawableRes)
        }.attach()
    }
}

data class FragmentItem(
    @DrawableRes val iconDrawableRes: Int,
    @StringRes val titleStringRes: Int,
    val createFragment: () -> Fragment
)