package com.example.shoppinglist.ui.shoppinglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.R
import com.example.shoppinglist.data.model.ShoppingList
import kotlinx.android.synthetic.main.layout_shopping_list_item.view.*

class ShoppingListAdapter(private val onClik: (ShoppingList)->Unit) : RecyclerView.Adapter<ShoppingListAdapter.ShopViewHolder>() {

    private var shopList = emptyList<ShoppingList>()

    class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        return ShopViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_shopping_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return shopList.size
    }

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) {
        val currentItem = shopList[position]
        holder.itemView.shoppingListTextView.text = currentItem.name
        holder.itemView.shoppingListStateTextView.text =
            "${holder.itemView.context.getText(R.string.shopping_list_state)} ${currentItem.countDone}/${currentItem.countAll}"
        holder.itemView.shoppingListItemLayout.setOnClickListener {
            onClik.invoke(currentItem)
        }

    }

    fun setData(shop: List<ShoppingList>) {
        this.shopList = shop
        notifyDataSetChanged()
    }

    fun getItem(position: Int): ShoppingList {
        return shopList[position]
    }
}
