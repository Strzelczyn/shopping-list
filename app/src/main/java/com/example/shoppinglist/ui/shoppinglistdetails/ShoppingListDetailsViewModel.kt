package com.example.shoppinglist.ui.shoppinglistdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shoppinglist.data.model.ShoppingList
import com.example.shoppinglist.data.model.ShoppingListDetails
import com.example.shoppinglist.data.repository.ShoppingListDetailsRepository
import com.example.shoppinglist.data.repository.ShoppingListRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ShoppingListDetailsViewModel() : ViewModel(), KoinComponent {

    lateinit var ShoppingListDetailsName: String

    lateinit var readAllData: LiveData<List<ShoppingListDetails>>

    var ShoppingListDetailsCount: Int = 1

    var ID: Int = 0

    val repository by inject<ShoppingListDetailsRepository>()


    val callShoppingListAddDetails: MutableLiveData<Boolean> = MutableLiveData(false)
    val callShoppingDetailsAddDialog: MutableLiveData<Boolean> = MutableLiveData(false)

    val callShoppingListDeleteDetails: MutableLiveData<ShoppingListDetails> = MutableLiveData(null)

    private val repositoryList by inject<ShoppingListRepository>()

    fun insertDataToDatabase() {
        callShoppingDetailsAddDialog.value = true
    }

    fun shoppingListAddCountAll(args: ShoppingList) {
        var item = args
        item.countAll = ++args.countAll
        updateShoppingList(item)
    }

    fun updateShoppingListDetailsByDone(item: ShoppingListDetails) {
        updateShoppingListDetails(item)
    }

    fun shoppingListDoneCountAll(item: ShoppingList, flagOfDone: Boolean) {
        if (flagOfDone) {
            item.countDone = ++item.countDone
        } else {
            item.countDone = --item.countDone
        }
        updateShoppingList(item)
    }

    fun shoppingListDetailsDelete(item: ShoppingListDetails) {
        callShoppingListDeleteDetails.value = item
        deleteShoppingListDetails(item)
    }

    fun shoppingListDetailsDeleteFromShoppingList(
        item: ShoppingList,
        details: ShoppingListDetails
    ) {
        item.countAll = --item.countAll
        if (details.done) {
            item.countDone = --item.countDone
        }
        updateShoppingList(item)
    }

    fun addListDetails() {
        val shoppingListDetails =
            ShoppingListDetails(0, ID, ShoppingListDetailsName, ShoppingListDetailsCount, false)
        addShoppingListDetails(shoppingListDetails)
        callShoppingListAddDetails.value = true
    }

    private fun updateShoppingList(item: ShoppingList) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryList.updateShoppingList(item)
        }
    }

    private fun deleteShoppingListDetails(item: ShoppingListDetails) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteShoppingListDetails(item)
        }
    }

    private fun updateShoppingListDetails(item: ShoppingListDetails) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateShoppingListDetails(item)
        }
    }

    private fun addShoppingListDetails(shoppingList: ShoppingListDetails) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addShoppingListDetails(shoppingList)
        }
    }
}