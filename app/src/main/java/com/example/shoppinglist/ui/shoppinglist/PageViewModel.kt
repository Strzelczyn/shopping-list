package com.example.shoppinglist.ui.shoppinglist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shoppinglist.data.model.ShoppingList
import com.example.shoppinglist.data.repository.ShoppingListRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.text.SimpleDateFormat
import java.util.*

class PageViewModel : ViewModel(), KoinComponent {

    lateinit var readAllData: LiveData<List<ShoppingList>>

    lateinit var ShoppingListName: String

    val repository by inject<ShoppingListRepository>()

    val callShoppingArchivedDialog: MutableLiveData<ShoppingList> =
        MutableLiveData<ShoppingList>(null)
    val callShoppingAddDialog: MutableLiveData<Boolean> = MutableLiveData<Boolean>(false)

    private val _index = MutableLiveData<Int>()

    fun setIndex(index: Int) {
        _index.value = index
    }

    fun getIndex(): Int? {
        return _index.value
    }

    fun insertDataToDatabase() {
        callShoppingAddDialog.value = true
    }

    fun addList() {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val shoppingList = ShoppingList(0, ShoppingListName, false, dateFormat.format(Date()), 0, 0)
        addShoppingList(shoppingList)
    }

    fun moveToArchived(item: ShoppingList) {
        item.archived = true
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateShoppingList(item)
        }
    }

    private fun addShoppingList(shoppingList: ShoppingList) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addShoppingList(shoppingList)
        }
    }
}