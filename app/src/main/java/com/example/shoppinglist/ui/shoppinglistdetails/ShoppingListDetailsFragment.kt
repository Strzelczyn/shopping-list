package com.example.shoppinglist.ui.shoppinglistdetails

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.shoppinglist.R
import com.example.shoppinglist.data.model.ShoppingListDetails
import com.example.shoppinglist.databinding.FragmentShoppingListDetailsBinding
import kotlinx.android.synthetic.main.custom_dialog_shopping_list_details.view.*
import kotlinx.android.synthetic.main.fragment_shopping_list_details.view.*

class ShoppingListDetailsFragment : Fragment() {

    private lateinit var viewModel: ShoppingListDetailsViewModel
    private lateinit var binding: FragmentShoppingListDetailsBinding

    private var dialog: AlertDialog? = null

    private val args: ShoppingListDetailsFragmentArgs by navArgs()

    private val adapter: ShoppingListDetailsAdapter = ShoppingListDetailsAdapter() { item ->
        viewModel.updateShoppingListDetailsByDone(item)
        viewModel.shoppingListDoneCountAll(args.curentShoppingList, item.done)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentShoppingListDetailsBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(ShoppingListDetailsViewModel::class.java)
        viewModel.readAllData = viewModel.repository.readAllDetailsByID(args.curentShoppingList.id)
        viewModel.ID = args.curentShoppingList.id
        binding.shoppinglistdetailsviewmodel = viewModel
        binding.lifecycleOwner = this
        binding.recyclerView.adapter = adapter
        adapter.archived = args.curentShoppingList.archived
        if (args.curentShoppingList.archived) {
            binding.root.fab.hide()
        }
        val hItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        context?.resources?.getDrawable(R.drawable.divider)?.let { hItemDecoration.setDrawable(it) }
        binding.recyclerView.addItemDecoration(hItemDecoration)
        if (!args.curentShoppingList.archived) {
            ShoppingListDetailsSwipeCallback(
                binding.root,
                adapter
            ) { item -> shopDetailsDeleteDialog(item) }.attachToRecyclerView(binding.recyclerView)
        }
        registerObserver()
        return binding.root
    }

    override fun onDestroy() {
        hideDialog()
        super.onDestroy()
    }

    private fun registerObserver() {
        viewModel.readAllData.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
        viewModel.callShoppingListAddDetails.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                viewModel.callShoppingListAddDetails.value = false
                viewModel.shoppingListAddCountAll(args.curentShoppingList)
            }
        })
        viewModel.callShoppingListDeleteDetails.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.callShoppingListDeleteDetails.value = null
                viewModel.shoppingListDetailsDeleteFromShoppingList(args.curentShoppingList, it)
            }
        })
        viewModel.callShoppingDetailsAddDialog.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                viewModel.callShoppingDetailsAddDialog.value = false
                shopDetailsAddDialog()
            }
        })
    }

    private fun shopDetailsDeleteDialog(shoppingListDetails: ShoppingListDetails) {
        hideDialog()
        dialog = AlertDialog.Builder(requireContext())
            .setPositiveButton(resources.getString(R.string.alert_dialog_positive)) { _, _ ->
                viewModel.shoppingListDetailsDelete(shoppingListDetails)
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_negative)) { _, _ ->
                adapter.notifyDataSetChanged()
            }
            .setTitle("${resources.getString(R.string.alert_dialog_title_delete)} ${shoppingListDetails.name}")
            .setMessage("${resources.getString(R.string.alert_dialog_message_delete)} ${shoppingListDetails.name}?")
            .create()
        dialog?.show()
    }

    private fun shopDetailsAddDialog() {
        hideDialog()
        val view = layoutInflater.inflate(R.layout.custom_dialog_shopping_list_details, null)
        dialog = AlertDialog.Builder(requireContext()).setView(view)
            .setPositiveButton(resources.getString(R.string.alert_dialog_add_positive)) { _, _ ->
                viewModel.ShoppingListDetailsName =
                    view.editTextShoppingListDetailsName.text.toString()
                if (view.editTextShoppingListDetailsCount.text.toString()
                        .isNotEmpty() && viewModel.ShoppingListDetailsName.isNotEmpty()
                ) {
                    try {
                        viewModel.ShoppingListDetailsCount =
                            view.editTextShoppingListDetailsCount.text.toString().toInt()
                        viewModel.addListDetails()
                    } catch (e: NumberFormatException) {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.bad_format_data),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.custom_dialog_message_bad_fields),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_add_negative)) { _, _ ->
            }
            .setTitle(resources.getString(R.string.alert_dialog_title_add_details))
            .setMessage(resources.getString(R.string.alert_dialog_message_data))
            .create()
        dialog?.show()
    }

    private fun hideDialog() {
        dialog?.dismiss()
        dialog = null
    }
}