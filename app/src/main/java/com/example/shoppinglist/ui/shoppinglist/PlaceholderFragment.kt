package com.example.shoppinglist.ui.shoppinglist

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.shoppinglist.R
import com.example.shoppinglist.data.model.ShoppingList
import com.example.shoppinglist.databinding.FragmentPlaceHolderBinding
import kotlinx.android.synthetic.main.custom_dialog_shopping_list.view.*
import kotlinx.android.synthetic.main.fragment_place_holder.view.*


class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel
    private lateinit var binding: FragmentPlaceHolderBinding

    private var dialog: AlertDialog? = null

    private val adapter: ShoppingListAdapter = ShoppingListAdapter(){item -> goToDetails(item)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProvider(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlaceHolderBinding.inflate(layoutInflater, container, false)
        binding.pageviewmodel = pageViewModel
        binding.lifecycleOwner = this
        val hItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        context?.resources?.getDrawable(R.drawable.divider)?.let { hItemDecoration.setDrawable(it) }
        binding.recyclerView.addItemDecoration(hItemDecoration)
        binding.recyclerView.adapter = adapter
        if (pageViewModel.getIndex() == 1) {
            pageViewModel.readAllData = pageViewModel.repository.readAllShoppingListArchived(false)
            binding.root.fab.show()
            ShoppingListSwipeCallback(
                binding.root,
                adapter
            ) {item -> shoppingListArchiveDialog(item)}.attachToRecyclerView(binding.recyclerView)
        } else {
            pageViewModel.readAllData = pageViewModel.repository.readAllShoppingListArchived(true)
            binding.root.fab.hide()
        }
        registerObserver()
        return binding.root
    }

    override fun onDestroy() {
        hideDialog()
        super.onDestroy()
    }

    private fun registerObserver() {
        pageViewModel.readAllData.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
        pageViewModel.callShoppingAddDialog.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                pageViewModel.callShoppingAddDialog.value = false
                shoppingListAddDialog()
            }
        })
    }

    private fun goToDetails(item: ShoppingList) {
        val action =
            ShoppingListFragmentDirections.actionShoppingListFragmentToShoppingListDetailsFragment(
                item
            )
        findNavController().navigate(action)
    }

    private fun shoppingListAddDialog() {
        hideDialog()
        val view = layoutInflater.inflate(R.layout.custom_dialog_shopping_list, null)
        dialog = AlertDialog.Builder(requireContext()).setView(view)
            .setPositiveButton(resources.getString(R.string.alert_dialog_add_positive)) { _, _ ->
                pageViewModel.ShoppingListName = view.editTextShoppingListName.text.toString()
                if (!pageViewModel.ShoppingListName.isEmpty()) {
                    pageViewModel.addList()
                } else {
                    Toast.makeText(
                        context,
                        resources.getString(R.string.custom_dialog_message_bad_field),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_add_negative)) { _, _ ->
            }
            .setTitle(resources.getString(R.string.alert_dialog_title_add))
            .setMessage(resources.getString(R.string.alert_dialog_message_data))
            .create()
        dialog?.show()
    }

    private fun shoppingListArchiveDialog(shoppingList: ShoppingList) {
        hideDialog()
        dialog = AlertDialog.Builder(requireContext())
            .setPositiveButton(resources.getString(R.string.alert_dialog_positive)) { _, _ ->
                pageViewModel.moveToArchived(shoppingList)
                pageViewModel.callShoppingArchivedDialog.value = null
            }
            .setNegativeButton(resources.getString(R.string.alert_dialog_negative)) { _, _ ->
                adapter.notifyDataSetChanged()
                pageViewModel.callShoppingArchivedDialog.value = null
            }
            .setTitle("${resources.getString(R.string.alert_dialog_title)} ${shoppingList.name}")
            .setMessage("${resources.getString(R.string.alert_dialog_message)} ${shoppingList.name}?")
            .create()
        dialog?.show()
    }

    private fun hideDialog() {
        dialog?.dismiss()
        dialog = null
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}