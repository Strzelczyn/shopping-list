package com.example.shoppinglist.ui.shoppinglist;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SectionsPagerAdapter extends FragmentStateAdapter {
    private final List<FragmentItem> fragmentItemList;

    public SectionsPagerAdapter(@NonNull @NotNull Fragment fragment, List<FragmentItem> fragmentItemList) {
        super(fragment);
        this.fragmentItemList = fragmentItemList;
    }

    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        return fragmentItemList.get(position).getCreateFragment().invoke();
    }

    @Override
    public int getItemCount() {
        return fragmentItemList.size();
    }
}
