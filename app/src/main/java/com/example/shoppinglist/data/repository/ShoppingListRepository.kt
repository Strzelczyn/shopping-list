package com.example.shoppinglist.data.repository

import androidx.lifecycle.LiveData
import com.example.shoppinglist.data.db.ShoppingListDao
import com.example.shoppinglist.data.model.ShoppingList

class ShoppingListRepository(private val shoppingListDao: ShoppingListDao) {

    suspend fun addShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.addShoppingList(shoppingList)
    }

    suspend fun updateShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.updateShoppingList(shoppingList)
    }

    suspend fun deleteShoppingList(shoppingList: ShoppingList) {
        shoppingListDao.deleteShoppingList(shoppingList)
    }

    fun readAllShoppingListArchived(archived: Boolean): LiveData<List<ShoppingList>> {
        return shoppingListDao.readAllShoppingListArchived(archived)
    }

}