package com.example.shoppinglist.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.shoppinglist.data.model.ShoppingList
import com.example.shoppinglist.data.model.ShoppingListDetails

@Database(
    entities = [ShoppingList::class, ShoppingListDetails::class],
    version = 1,
    exportSchema = false
)
abstract class ShoppingDatabase : RoomDatabase() {

    abstract fun shoppingListDao(): ShoppingListDao

    abstract fun shoppingListDetailsDao(): ShoppingListDetailsDao

    companion object {
        const val DB_NAME: String = "shopping_database"

        @Volatile
        private var INSTANCE: ShoppingDatabase? = null
        fun getDatabase(context: Context): ShoppingDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ShoppingDatabase::class.java,
                    DB_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}