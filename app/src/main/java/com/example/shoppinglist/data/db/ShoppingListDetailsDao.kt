package com.example.shoppinglist.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.shoppinglist.data.model.ShoppingListDetails

@Dao
interface ShoppingListDetailsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addShoppingListDetails(shoppingList: ShoppingListDetails)

    @Update
    suspend fun updateShoppingListDetails(shoppingList: ShoppingListDetails)

    @Delete
    suspend fun deleteShoppingListDetails(shoppingList: ShoppingListDetails)

    @Query("SELECT * FROM shopping_list_details_table WHERE shoppingListId = :ID")
    fun readAllDetailsByID(ID: Int): LiveData<List<ShoppingListDetails>>

}