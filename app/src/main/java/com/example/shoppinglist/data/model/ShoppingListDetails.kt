package com.example.shoppinglist.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "shopping_list_details_table")
class ShoppingListDetails(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val shoppingListId: Int,
    val name: String,
    val count: Int,
    var done: Boolean
) : Parcelable