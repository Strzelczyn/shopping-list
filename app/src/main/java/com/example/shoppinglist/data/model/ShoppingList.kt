package com.example.shoppinglist.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "shopping_list_table")
class ShoppingList(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val name: String,
    var archived: Boolean,
    val date: String,
    var countDone: Int,
    var countAll: Int
) : Parcelable