package com.example.shoppinglist.data.repository

import androidx.lifecycle.LiveData
import com.example.shoppinglist.data.db.ShoppingListDetailsDao
import com.example.shoppinglist.data.model.ShoppingListDetails

class ShoppingListDetailsRepository(private val shoppingListDetailsDao: ShoppingListDetailsDao) {

    suspend fun addShoppingListDetails(shoppingList: ShoppingListDetails) {
        shoppingListDetailsDao.addShoppingListDetails(shoppingList)
    }

    suspend fun updateShoppingListDetails(shoppingList: ShoppingListDetails) {
        shoppingListDetailsDao.updateShoppingListDetails(shoppingList)
    }

    suspend fun deleteShoppingListDetails(shoppingList: ShoppingListDetails) {
        shoppingListDetailsDao.deleteShoppingListDetails(shoppingList)
    }

    fun readAllDetailsByID(shopID: Int): LiveData<List<ShoppingListDetails>> {
        return shoppingListDetailsDao.readAllDetailsByID(shopID)
    }

}