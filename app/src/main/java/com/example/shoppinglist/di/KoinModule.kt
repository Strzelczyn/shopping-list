package com.example.shoppinglist.di


import com.example.shoppinglist.data.db.ShoppingDatabase
import com.example.shoppinglist.data.repository.ShoppingListDetailsRepository
import com.example.shoppinglist.data.repository.ShoppingListRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single {
        ShoppingListRepository(
            ShoppingDatabase.getDatabase(androidContext()).shoppingListDao()
        )
    }
    single {
        ShoppingListDetailsRepository(
            ShoppingDatabase.getDatabase(androidContext()).shoppingListDetailsDao()
        )
    }
}