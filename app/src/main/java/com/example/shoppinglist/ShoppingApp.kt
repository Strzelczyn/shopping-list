package com.example.shoppinglist

import android.app.Application
import com.example.shoppinglist.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ShoppingApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ShoppingApp)
            modules(repositoryModule)
        }
    }
}